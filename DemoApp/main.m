//
//  main.m
//  DemoApp
//
//  Created by pankaj_mac_mini on 18/08/16.
//  Copyright © 2016 Cssoft_mac_mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
