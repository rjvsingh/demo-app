//
//  AppDelegate.h
//  DemoApp
//
//  Created by pankaj_mac_mini on 18/08/16.
//  Copyright © 2016 Cssoft_mac_mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

